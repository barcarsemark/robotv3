*** Settings ***
Documentation     A resource file with reusable keywords and variables.
...
...               The system specific keywords created here form our own
...               domain specific language. They utilize keywords provided
...               by the imported SeleniumLibrary.
Library           SeleniumLibrary

*** Variables ***
${BROWSER}        Chrome
${DELAY}          0
${FB VALID USER}    mark.barcarse@gmail.com
${FB VALID PASSWORD}    harharhar07

*** Keywords ***
Go To FB Login Page
    Go To    https://www.facebook.com/login
    FB Login Page Should Be Open

Open Browser To FB Login Page
    Open Browser    https://www.facebook.com/    ${BROWSER}
    Maximize Browser Window
    Set Selenium Speed    ${DELAY}
    FB Landing Page Should Be Open

FB Landing Page Should Be Open
    Title Should Be    Facebook - Log In or Sign Up

FB Login Page Should Be Open
    Title Should Be    Log into Facebook | Facebook

Input FB Username
    [Arguments]    ${username}
    Input Text    email    ${username}

Input FB Password
    [Arguments]    ${password}
    Input Text    pass    ${password}

Submit FB Credentials
    Click Element    loginbutton

FB Welcome Page Should Be Open
    Location Should Be    https://www.facebook.com/