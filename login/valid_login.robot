*** Settings ***
Documentation     A test suite containing tests related to invalid login.
...
...               These tests are data-driven by their nature. They use a single
...               keyword, specified with Test Template setting, that is called
...               with different arguments to cover different scenarios.
...
...               This suite also demonstrates using setups and teardowns in
...               different levels.
Suite Setup       Open Browser To FB Login Page
Suite Teardown    Close Browser
Test Setup        Go To FB Login Page
Test Template     Login With Valid Credentials
Resource          resource.robot

*** Test Cases ***               USER NAME				PASSWORD
Valid Credentials                ${FB VALID USER}		${FB VALID PASSWORD}



*** Keywords ***
Login With Valid Credentials
	[Arguments]    ${username}    ${password}
	Input FB username   ${username}
    Input FB Password   ${password}
    Submit FB Credentials
    FB Welcome Page Should Be Open